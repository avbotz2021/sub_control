from dataclasses import dataclass

@dataclass
class State:
    x: float
    y: float
    z: float
    yaw: float
    pitch: float
    roll: float
